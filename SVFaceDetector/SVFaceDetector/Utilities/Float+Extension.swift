//
//  Float+Extension.swift
//  FaceDetection
//
//  Created by Srinivas Rao Kurakula on 09/03/22.
//

import UIKit

extension CGFloat {

    var detectSmile: EmotionStatus {
        if self > 0.7 {
            return .smiling
        } else if self > 0.2 {
            return .nuetral
        } else {
            return .notSmiling
        }
    }

    var detectHeadPosition: HeadPosition {
        if self > 10 {
            return .right
        } else if self < -10 {
            return .left
        } else {
            return .straight
        }
    }
}

