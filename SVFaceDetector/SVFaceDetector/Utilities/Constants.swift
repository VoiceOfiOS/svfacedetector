//
//  Constants.swift
//  VIFaceDetector
//
//  Created by Srinivas Rao Kurakula on 16/03/22.
//

import UIKit

let kSessionQueueLabel = "com.voiceofios.VIFaceDetector.SessionQueue"
let kVideoDataOutputQueueLabel = "com.voiceofios.VIFaceDetector.VideoDataOutputQueue"
let kAudioDataOutputQueueLabel = "com.voiceofios.VIFaceDetector.AudioDataOutputQueue"

let KRectangleViewCornerRadius: CGFloat = 10.0
let kRectangleViewAlpha: CGFloat = 0.3
let kOriginalScale: CGFloat = 1.0

let kStartRecording = "Start Recording"
let kStopRecording = "Stop Recording"

let kCountDownValue = 5

