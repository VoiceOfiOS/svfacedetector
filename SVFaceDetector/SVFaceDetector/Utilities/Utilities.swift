//
//  Utilities.swift
//  VIFaceDetector
//
//  Created by Srinivas Rao Kurakula on 15/03/22.
//

import UIKit

struct Utilities {
    
    static func fetchFileLocationPath(name: String) -> URL {
        let documentDirURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let fileURL = documentDirURL?.appendingPathComponent("\(name)").appendingPathExtension("mov")

        return fileURL!
    }

    static func removeMovie(fileURL: URL) {
        do {
            if FileManager.default.fileExists(atPath: fileURL.path) {
                try FileManager.default.removeItem(at: fileURL)
                print("file removed")
            }
        } catch {
            print(error)
        }
    }

    static func detectEyesStatus(left: CGFloat, right: CGFloat) -> EyeStatus {
        if right > 0.1 && left > 0.1 {
            return .open
        } else {
            return .close
        }
    }
}
