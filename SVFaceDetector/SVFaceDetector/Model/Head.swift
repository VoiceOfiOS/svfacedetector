//
//  Head.swift
//  FaceDetection
//
//  Created by Srinivas Rao Kurakula on 09/03/22.
//

import Foundation

enum HeadPosition {
    case left, right, straight

    var description: String {
        switch self {
        case .left:
            return "Left"
        case .right:
            return "Right"
        default:
            return "Straight"
        }
    }
}

class Head {
    var total: Int = 0
    var straightCount: Int = 0
    var leftCount: Int = 0
    var rightCout: Int = 0

    var json: [String: Int] {
        return ["total": total, "headstraight": straightCount, "headleft": leftCount, "headright": rightCout]
    }
}
