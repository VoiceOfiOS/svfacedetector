//
//  Emotion.swift
//  FaceDetection
//
//  Created by Srinivas Rao Kurakula on 09/03/22.
//

import Foundation

enum EmotionStatus {
    case smiling, notSmiling, nuetral

    var description: String {
        switch self {
        case .smiling:
            return "Smiling"
        case .notSmiling:
            return "Not Smiling"
        default:
            return "Neutral"
        }
    }
}

class Emotion {
    var total: Int = 0
    var smile: Int = 0
    var notSmile: Int = 0
    var neutral: Int = 0

    var json: [String: Int] {
        return ["total": total, "smile": smile, "notsmile": notSmile, "neutral": neutral]
    }
}
