//
//  Eye.swift
//  FaceDetection
//
//  Created by Srinivas Rao Kurakula on 09/03/22.
//

import Foundation

enum EyeStatus {
    case open, close

    var description: String {
        switch self {
        case .open:
            return "Eyes opened"
        case .close:
            return "Eyes closed"
        }
    }
}

class Eye {
    var total: Int = 0
    var open: Int = 0
    var close: Int = 0

    var json: [String: Int] {
        return ["total": total, "eyesopen": open, "eyesclose": close]
    }
}
