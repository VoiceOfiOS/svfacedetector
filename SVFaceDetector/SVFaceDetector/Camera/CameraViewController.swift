//
//  CameraViewController.swift
//  VIFaceDetector
//
//  Created by Srinivas Rao Kurakula on 15/03/22.
//

import UIKit
import AVKit
import MLKit

private enum RecordingState {
    case preparation, start, stop
}

public final class CameraViewController: UIViewController {

    //MARK: IBOutlets
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var facialResultsView: UIView!
    @IBOutlet weak var eyeLabel: UILabel!
    @IBOutlet weak var headPositionLabel: UILabel!
    @IBOutlet weak var smileLabel: UILabel!
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var recordingButton: UIButton!

    //MARK: Audio & Video Settings
    private lazy var sessionQueue = DispatchQueue(label: kSessionQueueLabel)
    private lazy var captureSession = AVCaptureSession()
    private var previewLayer: AVCaptureVideoPreviewLayer!

    private lazy var previewOverlayView: UIImageView = {
        precondition(isViewLoaded)
        let previewOverlayView = UIImageView(frame: .zero)
        previewOverlayView.contentMode = UIView.ContentMode.scaleAspectFill
        previewOverlayView.translatesAutoresizingMaskIntoConstraints = false
        return previewOverlayView
    }()

    private lazy var annotationOverlayView: UIView = {
        precondition(isViewLoaded)
        let annotationOverlayView = UIView(frame: .zero)
        annotationOverlayView.translatesAutoresizingMaskIntoConstraints = false
        return annotationOverlayView
    }()

    //Asset writer for video and audio frames writing on to the specified path.
    private var assetWriter: AVAssetWriter?
    private var videoWriterInput: AVAssetWriterInput?
    private var audioWriterInput: AVAssetWriterInput?
    private var sessionAtSourceTime: CMTime?

    //Producing audio and video outputs from camera
    private var videoDataOutput = AVCaptureVideoDataOutput()
    private var audioDataOutput = AVCaptureAudioDataOutput()

    //MARK: Constants
    private let fileName = UUID().uuidString
    private var lastFrame: CMSampleBuffer?
    private var timer: Timer?
    private var currentTime = kCountDownValue
    private var recordingState: RecordingState = .preparation

    // Facial Status
    private var eye = Eye()
    private var head = Head()
    private var emotion = Emotion()

    public var onCompletion: ((URL, [String: Any]) -> Void)?

    //MARK: ViewLife Cycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startSession()
    }

    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopSession()
    }

    public static let shared = CameraViewController(nibName: "CameraViewController", bundle:
                                                        Bundle(for:CameraViewController.self))

    private init() {
        super.init(nibName: String(describing: CameraViewController.self), bundle:  Bundle(for:CameraViewController.self))
    }

    private override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @IBAction func didSelectStartOrStopRecording(_ sender: Any) {
        switch recordingState {
        case .preparation:
            recordingButton.isEnabled = false
            startCountDown()
        case .start:
            recordingState = .stop
            stopSession()
            videoWriterInput?.markAsFinished()
            audioWriterInput?.markAsFinished()
            assetWriter?.finishWriting { [weak self] in
                self?.sessionAtSourceTime = nil
                self?.updateResults()
            }
        default:
            break;
        }
    }

    func updateResults() {
        let dictionary = [
            "metrics": [
                "head_orientation_v0.2": eye.json,
                "head_pose_v0.2": head.json,
                "emotion_v0.2": emotion.json
            ]
        ]

        let url =  Utilities.fetchFileLocationPath(name: fileName)

        DispatchQueue.main.async {
            if let navController = self.navigationController {
                self.onCompletion?(url, dictionary)
                navController.popViewController(animated: true)
            } else {
                self.dismiss(animated: true) {
                    self.onCompletion?(url, dictionary)
                }
            }
        }
    }
}

private extension CameraViewController {

    func setupViews() {
        countdownLabel.text = "\(kCountDownValue)"
        countdownLabel.isHidden = true

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        setUpPreviewOverlayView()
        setUpAnnotationOverlayView()

        setUpCaptureSessionInput()
        setUpCaptureSessionOutput()

        initAssetWriter()
    }

    func setUpPreviewOverlayView() {
        cameraView.insertSubview(previewOverlayView, at: 0)
        NSLayoutConstraint.activate([
            previewOverlayView.centerXAnchor.constraint(equalTo: cameraView.centerXAnchor),
            previewOverlayView.centerYAnchor.constraint(equalTo: cameraView.centerYAnchor),
            previewOverlayView.leadingAnchor.constraint(equalTo: cameraView.leadingAnchor),
            previewOverlayView.trailingAnchor.constraint(equalTo: cameraView.trailingAnchor),
        ])
    }

    func setUpAnnotationOverlayView() {
        cameraView.addSubview(annotationOverlayView)
        NSLayoutConstraint.activate([
            annotationOverlayView.topAnchor.constraint(equalTo: cameraView.topAnchor),
            annotationOverlayView.leadingAnchor.constraint(equalTo: cameraView.leadingAnchor),
            annotationOverlayView.trailingAnchor.constraint(equalTo: cameraView.trailingAnchor),
            annotationOverlayView.bottomAnchor.constraint(equalTo: cameraView.bottomAnchor),
        ])
    }

    func setUpCaptureSessionInput() {
        weak var weakSelf = self
        sessionQueue.async {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }

            guard let videoDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else {
                return
            }

            do {
                strongSelf.captureSession.beginConfiguration()

                let input = try AVCaptureDeviceInput(device: videoDevice)
                guard strongSelf.captureSession.canAddInput(input) else {
                    print("Failed to add capture session video input.")
                    return
                }
                strongSelf.captureSession.addInput(input)

                guard let microphoneDevice = AVCaptureDevice.default(.builtInMicrophone, for: .audio, position: .unspecified) else {
                    return
                }
                let audioInput = try AVCaptureDeviceInput(device: microphoneDevice)
                guard strongSelf.captureSession.canAddInput(audioInput) else {
                    print("Failed to add capture session audio input.")
                    return
                }
                strongSelf.captureSession.addInput(audioInput)

                strongSelf.captureSession.commitConfiguration()
            } catch {
                print("Failed to create capture device input: \(error.localizedDescription)")
            }
        }
    }

    func setUpCaptureSessionOutput() {
        weak var weakSelf = self
        sessionQueue.async {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            strongSelf.captureSession.beginConfiguration()
            // When performing latency tests to determine ideal capture settings,
            // run the app in 'release' mode to get accurate performance metrics
            strongSelf.captureSession.sessionPreset = AVCaptureSession.Preset.medium

            // Video
            strongSelf.videoDataOutput = AVCaptureVideoDataOutput()
            strongSelf.videoDataOutput.videoSettings = [
                (kCVPixelBufferPixelFormatTypeKey as String): kCVPixelFormatType_32BGRA
            ]
            strongSelf.videoDataOutput.alwaysDiscardsLateVideoFrames = true

            let outputQueue = DispatchQueue(label: kVideoDataOutputQueueLabel)
            strongSelf.videoDataOutput.setSampleBufferDelegate(strongSelf, queue: outputQueue)
            guard strongSelf.captureSession.canAddOutput(strongSelf.videoDataOutput) else {
                print("Failed to add capture session output.")
                return
            }
            strongSelf.captureSession.addOutput(strongSelf.videoDataOutput)

            // Audio
            strongSelf.audioDataOutput = AVCaptureAudioDataOutput()
            let audioOutputQueue = DispatchQueue(label: kAudioDataOutputQueueLabel)
            strongSelf.audioDataOutput.setSampleBufferDelegate(strongSelf, queue: audioOutputQueue)
            guard strongSelf.captureSession.canAddOutput(strongSelf.audioDataOutput) else {
                print("Failed to add capture session output.")
                return
            }
            strongSelf.captureSession.addOutput(strongSelf.audioDataOutput)

            strongSelf.captureSession.commitConfiguration()
        }
    }

    func startSession() {
        weak var weakSelf = self
        sessionQueue.async {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            strongSelf.captureSession.startRunning()
        }
    }

    func stopSession() {
        weak var weakSelf = self
        sessionQueue.async {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            strongSelf.captureSession.stopRunning()
        }
    }

    func initAssetWriter() {
        do {
            let url =  Utilities.fetchFileLocationPath(name: fileName)
            Utilities.removeMovie(fileURL: url)
            assetWriter = try AVAssetWriter(outputURL: url, fileType: .mov)

            guard let assetWriter = assetWriter else {
                return
            }

            videoWriterInput = AVAssetWriterInput(mediaType: .video, outputSettings: videoDataOutput.recommendedVideoSettingsForAssetWriter(writingTo: .mov))
            videoWriterInput?.expectsMediaDataInRealTime = true
            videoWriterInput?.transform = CGAffineTransform(rotationAngle: .pi/2)
            assetWriter.add(videoWriterInput!)

            audioWriterInput = AVAssetWriterInput(mediaType: .audio, outputSettings: nil)
            audioWriterInput?.expectsMediaDataInRealTime = true
            assetWriter.add(audioWriterInput!)

        } catch {
            debugPrint("Asset writer failed")
        }
    }

    func canWrite() -> Bool {
        return  assetWriter != nil && assetWriter?.status == .writing
    }

    func startCountDown() {
        currentTime = kCountDownValue
        countdownLabel.text = "\(currentTime)"
        countdownLabel.isHidden = false
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCountDownValue), userInfo: nil, repeats: true)
    }

    @objc func updateCountDownValue() {
        currentTime -= 1
        countdownLabel.text = "\(currentTime)"
        if currentTime == 0 {
            countdownLabel.isHidden = true
            resetTimer()
            recordingButton.setTitle(kStopRecording, for: .normal)
            recordingButton.isEnabled = true
            recordingState = .start
            sessionAtSourceTime = nil
            self.assetWriter?.startWriting()
        }
    }

    func resetTimer() {
        timer?.invalidate()
        timer = nil
    }
}

extension CameraViewController: AVCaptureAudioDataOutputSampleBufferDelegate, AVCaptureVideoDataOutputSampleBufferDelegate {

    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {

        if recordingState == .start {
            if canWrite(), sessionAtSourceTime == nil {
                sessionAtSourceTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
                assetWriter?.startSession(atSourceTime: sessionAtSourceTime!)
            }

            if canWrite(), output.isKind(of: AVCaptureVideoDataOutput.self), videoWriterInput?.isReadyForMoreMediaData == true {
                videoWriterInput?.append(sampleBuffer)
            }

            if canWrite(), output.isKind(of: AVCaptureAudioDataOutput.self), audioWriterInput?.isReadyForMoreMediaData == true {
                audioWriterInput?.append(sampleBuffer)
            }
        }

        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }

        lastFrame = sampleBuffer
        let visionImage = VisionImage(buffer: sampleBuffer)
        let orientation = UIUtilities.imageOrientation(fromDevicePosition: .front)
        visionImage.orientation = orientation

        guard let inputImage = MLImage(sampleBuffer: sampleBuffer) else {
            return
        }
        inputImage.orientation = orientation

        //
        let imageWidth = CGFloat(CVPixelBufferGetWidth(imageBuffer))
        let imageHeight = CGFloat(CVPixelBufferGetHeight(imageBuffer))

        detectFacesOnDevice(in: visionImage, width: imageWidth, height: imageHeight)
    }

}

//MARK: Face Detection
private extension CameraViewController  {

    func detectFacesOnDevice(in image: VisionImage, width: CGFloat, height: CGFloat) {
        // When performing latency tests to determine ideal detection settings, run the app in 'release'
        // mode to get accurate performance metrics.
        let options = FaceDetectorOptions()
        options.landmarkMode = .none
        options.contourMode = .all
        options.classificationMode = .all
        options.performanceMode = .fast
        let faceDetector = FaceDetector.faceDetector(options: options)
        var faces: [Face]
        do {
            faces = try faceDetector.results(in: image)
        } catch let error {
            print("Failed to detect faces with error: \(error.localizedDescription).")
            self.updatePreviewOverlayViewWithLastFrame()
            return
        }
        self.updatePreviewOverlayViewWithLastFrame()
        guard !faces.isEmpty else {
            print("On-Device face detector returned no results.")
            return
        }
        weak var weakSelf = self
        DispatchQueue.main.sync {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            for face in faces {
                let normalizedRect = CGRect(
                    x: face.frame.origin.x / width,
                    y: face.frame.origin.y / height,
                    width: face.frame.size.width / width,
                    height: face.frame.size.height / height
                )
                let standardizedRect = strongSelf.previewLayer.layerRectConverted(
                    fromMetadataOutputRect: normalizedRect
                ).standardized
                UIUtilities.addRectangle(standardizedRect, to: strongSelf.annotationOverlayView, color: .green)

                strongSelf.updateFacialResults(face: face)
            }
        }
    }

    func updatePreviewOverlayViewWithLastFrame() {
        weak var weakSelf = self
        DispatchQueue.main.sync {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            guard let lastFrame = lastFrame,
                  let imageBuffer = CMSampleBufferGetImageBuffer(lastFrame)
            else {
                return
            }
            strongSelf.updatePreviewOverlayViewWithImageBuffer(imageBuffer)
            strongSelf.removeDetectionAnnotations()
        }
    }

    func updatePreviewOverlayViewWithImageBuffer(_ imageBuffer: CVImageBuffer?) {
        guard let imageBuffer = imageBuffer else {
            return
        }
        let orientation: UIImage.Orientation = .leftMirrored
        let image = UIUtilities.createUIImage(from: imageBuffer, orientation: orientation)
        previewOverlayView.image = image
    }

    func removeDetectionAnnotations() {
        for annotationView in annotationOverlayView.subviews {
            annotationView.removeFromSuperview()
        }
    }

    func updateFacialResults(face: Face) {
        if  face.hasSmilingProbability {
            let status = face.smilingProbability.detectSmile
            smileLabel.text = status.description
            if recordingState == .start {
                switch status {
                case .smiling:
                    emotion.smile += 1
                case .notSmiling:
                    emotion.notSmile += 1
                default:
                    emotion.neutral += 1
                }
                emotion.total += 1
            }
        }

        if face.hasLeftEyeOpenProbability && face.hasRightEyeOpenProbability {
            let leftEye = face.leftEyeOpenProbability
            let rightEye = face.rightEyeOpenProbability
            let eyeStatus = Utilities.detectEyesStatus(left: leftEye, right: rightEye)
            eyeLabel.text = eyeStatus.description
            if recordingState == .start {
                switch eyeStatus {
                case .open:
                    eye.open += 1
                default:
                    eye.close += 1
                }
                eye.total += 1
            }
        }

        if face.hasHeadEulerAngleY {
            let headPosition = face.headEulerAngleY.detectHeadPosition
            headPositionLabel.text = headPosition.description
            if recordingState == .start {
                switch headPosition {
                case .left:
                    head.leftCount += 1
                case .right:
                    head.rightCout += 1
                default:
                    head.straightCount += 1
                }
                head.total += 1
            }
        }
    }
}
