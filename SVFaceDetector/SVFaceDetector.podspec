#
#  Be sure to run `pod spec lint SVFaceDetector.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

spec.name         = "SVFaceDetector"

spec.version      = "0.0.9"

spec.summary      = "To detect the facial expressions."

spec.description  = "We are using AVFoundation and Google MLKit for capturing video frames and identify the facial expressions"

spec.license = { :type => "MIT", :file => "SVFaceDetector/LICENSE.md" }

spec.homepage     = "https://VoiceOfiOS@bitbucket.org/VoiceOfiOS/svfacedetector"

spec.author       = { "Srinivas" => "srinivasu.kurakula@gmail.com" }

spec.source       = { :git => "https://VoiceOfiOS@bitbucket.org/VoiceOfiOS/svfacedetector.git", :tag => spec.version.to_s, :submodules => true }

spec.source_files  = "SVFaceDetector/**/*.{swift, plist}"

spec.resources = "SVFaceDetector/**/*.{xib}"

spec.frameworks = 'UIKit', 'Foundation', 'AVFoundation', 'MLKit'

spec.dependency 'GoogleMLKit/FaceDetection', '2.6.0'

spec.platform = :ios, '13.0'

spec.ios.deployment_target = '13.0'

spec.swift_version = '5.0'
end
